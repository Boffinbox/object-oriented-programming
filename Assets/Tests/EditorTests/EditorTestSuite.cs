﻿using UnityEngine;
using UnityEngine.TestTools;
using UnityEditor;
using NUnit.Framework;
using System.Collections;
using BallClickerGame;
using FluentAssertions;

public class EditorTestSuite
{
    private GameObject testEnv;
    private RectTransform testTarget;
    private GameObject testSubject;

    [SetUp]
    public void SetUp()
    {
        // this is called before each test ..
        testSubject = Object.Instantiate(Resources.Load("Tests/character") as GameObject);
        testEnv = Object.Instantiate(Resources.Load("Tests/TestEnvironment") as GameObject);
        testTarget = testEnv.transform.GetChild(0) as RectTransform;

    }

    [TearDown]
    public void TearDown()
    {
        Object.DestroyImmediate(testEnv);
        Object.DestroyImmediate(testSubject);
    }

    [Test]
    public void TestRandomColorGeneration()
    {
        Random.InitState(10);
        var c = CharacterValues.GenerateRandomColors();
        c.r.Should().BeApproximately(0.611f, 0.001f);
        c.g.Should().BeApproximately(0.235f, 0.001f);
        c.b.Should().BeApproximately(0.847f, 0.001f);
    }

    [Test]
    public void VerifyThatSeedIsUsedForRandomNumbers()
    {
        Random.InitState(10);
        var c = CharacterValues.GenerateRandomColors();
        Random.InitState(10);
        var d = CharacterValues.GenerateRandomColors();
        c.Should().Be(d);
    }

    [Test]
    public void VerifyThatDifferentSeedGivesDifferentResult()
    {
        Random.InitState(10);
        var c = CharacterValues.GenerateRandomColors();
        Random.InitState(20);
        var d = CharacterValues.GenerateRandomColors();
        c.Should().NotBe(d);
    }

    [Test]
    public void CheckCharacterInstantiationPositionIsAtOrigin()
    {
        Generator.RepositionCharacter(testSubject);
        testSubject.GetComponent<RectTransform>().anchoredPosition.Should().Be(new Vector2 (0, 0));
    }

    [Test]
    public void CheckCharacterInstantiationFromPrefab()
    {
        Generator.CharacterCreate(testSubject, 1, testTarget);
        testTarget.childCount.Should().Be(1);
    }

    //[Test]
    //public void CheckInstantiatedCharacterIsTheSameAsPrefab()
    //{
    //    Generator.CharacterCreate(testSubject, 2, testTarget);
    //}
}