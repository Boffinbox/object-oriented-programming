﻿namespace BallClickerGame
{
    // we declare a static class to ensure that the experience value can be
    // accessed anywhere. this is a lazy and dangerous approach, quite frankly,
    // but it's useful for this demonstration. consider a singleton
    // pattern in the future.
    public static class ExperienceValue
    {
        public static int Experience
        {
            // A getter is used to make a field readable
            get
            {
                return experience;
            }
            // ...and a setter is used to make the field writable
            set
            {
                experience = value;
            }
        }
        private static int experience;
    }
}