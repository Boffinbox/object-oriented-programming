﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BallClickerGame
{
    public class CharacterValues : MonoBehaviour
    {
        // Here we declare a list of properties to use.
        // These properties can be read and written to.
        // These are written in a short form.
        // To examine a long form, check the
        // ExperienceValue.cs script.
        private string CharacterName { get; set; }

        private int CharacterExperienceValue { get; set; }

        private int CharacterHealthValue { get; set; }

        private Text CharacterNameText { get; set; }

        private Text CharacterHealthText { get; set; }

        private Color CharacterColor { get; set; }

        protected void SetColor()
        {
            Color chosenColor = GenerateRandomColors();
            CharacterColor = MakeColorsBrighter(chosenColor);
            GetComponentInChildren<Image>().color = CharacterColor;
            ColorNamer();
        }
        public static Color GenerateRandomColors()
        {
            float[] colorRandomValue = new float[3];
            for (int i = 0; i < colorRandomValue.Length; i++)
            {
                int randomColorRaw = UnityEngine.Random.Range(50, 225);
                colorRandomValue[i] = randomColorRaw / 255f;
            }

            return new Color(colorRandomValue[0], colorRandomValue[1], colorRandomValue[2], 1);
        }
        private static Color MakeColorsBrighter(Color unsaturatedColor)
        {
            float H, S, V;
            Color.RGBToHSV(unsaturatedColor, out H, out S, out V);
            V = 1f;
            if (S <= 0.15f)
            {
                S = 0f;
                V = 0.6f;
            }
            else
            {
                S = 1f;
            }

            var c = Color.HSVToRGB(H, S, V);
            return c;
        }
        void ColorNamer()
        {
            // We split our colour out into components again.
            float H, S, V;
            Color.RGBToHSV(CharacterColor, out H, out S, out V);
            // We already know that colours with a sat lower than 0.15 are grey, but we must name it.
            if (S <= 0.2f)
            {
                CharacterName = "Grey";
            }
            // If it's not grey, fetch the colour name and assign it.
            // We pass our hue value to "MapColor" so it can peform a lookup.
            else
            {
                CharacterName = MapColor(H);
            }
            // Pass the selected colour name to a name assigning method.
            DrawValue(CharacterName);
        }
        string MapColor(float H)
        {
            // Take a list of the colour mappings, and check our hue value against each item in the list.
            // If our colour is less than or equal to the detected value, assign the chosen name.
            // Only when the next colour in the list exceeds our current hue, do we reject it.
            // This approach works because every colour has some decimal representation as a hue.
            // If we tried to figure this out from RGB, we would be here a long time.
            foreach (var m in mappings)
            {
                if (H <= m.Hue)
                {
                    return m.ColorName;
                }
            }
            return "???";
        }
        // An array of colour mappings that are plugged into our struct.
        // It is constructed to be read only, because these should never change in runtime.
        // Red is always going to be red, blue always blue, etc
        static HueToColorMapping[] mappings = new HueToColorMapping[]
        {
        new HueToColorMapping(0.05f, "Red"),
        new HueToColorMapping(0.15f, "Orange"),
        new HueToColorMapping(0.22f, "Yellow"),
        new HueToColorMapping(0.33f, "Lime"),
        new HueToColorMapping(0.46f, "Green"),
        new HueToColorMapping(0.57f, "Light Blue"),
        new HueToColorMapping(0.72f, "Blue"),
        new HueToColorMapping(0.80f, "Violet"),
        new HueToColorMapping(0.92f, "Magenta"),
        new HueToColorMapping(1.00f, "Red"),
        };

        struct HueToColorMapping
        {
            public readonly string ColorName;
            public readonly float Hue;

            public HueToColorMapping(float hue, string colorName)
            {
                ColorName = colorName;
                Hue = hue;
            }
        }
        // These next methods allow inherited classes to set experience, health and texts.
        protected void SetExperience(int experienceAmount)
        {
            CharacterExperienceValue = experienceAmount;
        }
        protected void SetHealth(int healthAmount)
        {
            CharacterHealthValue = healthAmount;
            DrawValue(CharacterHealthValue);
        }
        protected void SetTexts(Text nameText, Text healthText)
        {
            CharacterNameText = nameText;
            CharacterHealthText = healthText;
        }
        // This method is marked as virtual so it can be overwritten by inherited classes
        // We give it a value anyway in case there is no inheritance, for whatever reason
        protected virtual void SetVelocity()
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        }
        // There is some extraneous code here, but it could be useful to
        // have the changeAmount permanently stored, in case it needs
        // to be acted upon by another method in this class in the future.
        protected void ChangeHealth(int changeAmount)
        {
            CharacterHealthValue += changeAmount;
            DrawValue(CharacterHealthValue);
            if (CharacterHealthValue <= 0)
            {
                // ExperienceValue is a static property in a static class
                // This means we can directly call it without instantiation, since
                // there is only ever one instance.
                ExperienceValue.Experience += CharacterExperienceValue;
                // This next line is what you would have to do if you haven't marked it as static
                GameObject.FindGameObjectWithTag("GameController").GetComponent<ExperienceUIDrawing>().DrawValues();
                // We output the random experience value to console, just to verify that it's working.
                Debug.Log(ExperienceValue.Experience.ToString());
                // Then, get rid of the ball.
                // This is inefficient, and object pooling would be better
                // but, because there are a small amount of objects,
                // this is more cost effective
                Destroy(gameObject);
            }
        }
        // Example of method overloading
        // DrawValue works whether we feed in a string or an integer
        private void DrawValue(string value)
        {
            CharacterNameText.text = value;
        }
        private void DrawValue(int value)
        {
            CharacterHealthText.text = value.ToString();
        }
    }
}