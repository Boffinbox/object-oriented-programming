﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BallClickerGame
{
    public class ExperienceUIDrawing : MonoBehaviour
    {
        // define some text fields in the inspector
        [SerializeField] Text experienceText;
        [SerializeField] Text levelText;
        [SerializeField] Text noobProText;
        // we call drawvalues once on startup to initialize some real text values
        private void Start()
        {
            DrawValues();
        }
        // the DrawValues method is actually three methods stacked on top of each other wearing a trenchcoat and a hat
        public void DrawValues()
        {
            ExperienceTextDraw();
            LevelTextDraw();
            CheckIfPlayerIsAPro();
        }
        void ExperienceTextDraw()
        {
            // convert the experience property into a string, and tack it onto a label
            experienceText.text = "Experience: " + ExperienceValue.Experience.ToString();
        }
        void LevelTextDraw()
        {
            // ditto but for level. we derive level as a hundreth of the experience value
            // you ensure that you can't have fractional levels by rounding to floor
            levelText.text = "Level: " + (Mathf.Floor(ExperienceValue.Experience / 100)).ToString();
        }
        void CheckIfPlayerIsAPro()
        {
            // this is an example use of the conditional operator
            // it's basically a one line if/else statement
            // useful for quick implementation
            string noobMessage = ExperienceValue.Experience < 250 ? "Noob." : "Pro!";
            // not really sure why i'm null checking this one, and not the other twoW
            if (noobMessage != null)
            {
                noobProText.text = noobMessage;
            }
        }
    }
}