﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BallClickerGame
{
    public class Generator : MonoBehaviour
    {
        // We assign a ball and square prefab in the inspector
        // and also identify the canvas.
        // I made the game work entirely using canvas elements,
        // only because it's a small project
        [SerializeField] GameObject ballPrefab;
        [SerializeField] GameObject squarePrefab;
        [SerializeField] RectTransform canvas;

        private void Start()
        {
            // A method that recieves a game object to instantiate, and the amount
            CharacterCreate(ballPrefab, 10, canvas);
            CharacterCreate(squarePrefab, 3, canvas);
        }

        public static void CharacterCreate(GameObject character, int amount, RectTransform parentTransform)
        {
            if (character == null)
            {
                Debug.Log("CharacterCreate method is passing a null prefab. Is this intentional?");
            }
            for (int i = 1; i <= amount; i++)
            {
                var newCharacter = Instantiate(character, parentTransform);
                RepositionCharacter(newCharacter);
            }
        }

        public static void RepositionCharacter(GameObject newCharacter)
        {
            var characterRectTransform = newCharacter.GetComponent<RectTransform>();
            characterRectTransform.anchoredPosition = new Vector2(0, 0);
        }
    }
}