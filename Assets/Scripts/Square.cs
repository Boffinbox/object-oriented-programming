﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BallClickerGame
{
    public class Square : CharacterValues
    {
        // this script is basically a duplicate of ball.cs
        // this is bad form, because the code in both scripts
        // could be refactored into one script
        [SerializeField] Text nameText;
        [SerializeField] Text healthText;

        void Start()
        {
            SetTexts(nameText, healthText);
            SetColor();
            SetHealth(Random.Range(100, 500));
            SetExperience(Random.Range(50, 100));
            SetVelocity();
        }
        void OnClick()
        {
            ChangeHealth(-30);
        }
        protected override void SetVelocity()
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(Random.Range(-20, 20), Random.Range(-20, 20));
        }
    }
}