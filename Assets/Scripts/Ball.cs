﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BallClickerGame
{
    public class Ball : CharacterValues
    {
        // we assign two ui texts to the ball, that are passed up to the charactervalues class
        [SerializeField] Text nameText;
        [SerializeField] Text healthText;

        void Start()
        {
            // we pass up the texts here
            SetTexts(nameText, healthText);
            // sets colour...
            SetColor();
            // we call the methods in this script instead of character values because it allows
            // us to choose custom values for the random ranges
            SetHealth(Random.Range(5, 250));
            SetExperience(Random.Range(26, 50));
            SetVelocity();
        }
        void OnClick()
        {
            ChangeHealth(-50);
        }
        // example of method *overriding* (not overloading)
        // in charactervalues.cs, we have a virtual SetVelocity()
        protected override void SetVelocity()
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(Random.Range(-200, 200), Random.Range(-200, 200));
        }
    }
}